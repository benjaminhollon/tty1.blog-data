title: Anatomy of a Command
whoami: benjamin
published: Sat Mar 25 02:26:31 PM CDT 2023
mastodonThread: https://fosstodon.org/@benjaminhollon/110085603876537305
tags:
- read-first
- basics

---

For the most part, this blog will not be dealing with the very basics. I find it safe to assume that most people interested in a blog about the Linux terminal already have some degree of fundamental knowledge about it.

That said, it's a good idea to have a grounding place we can all start off from. Let's take a command and dissect it, looking at its parts and learning what they mean.

---

For the most part, this blog will not be dealing with the very basics. I find it safe to assume that most people interested in a blog about the Linux terminal already have some degree of fundamental knowledge about it.

That said, it's a good idea to have a grounding place we can all start off from. Let's take a command and dissect it, looking at its parts and learning what they mean.

## Anatomy of a Command

Lets start off with this simple command:

```bash
$ ls --almost-all --sort=extension ~/downloads
```

Whoa, whoa, whoa, what's going on here? So much. If you're not very used to the terminal, this is a perplexing line of gibberish. Let's break it down.

### `$` - The Prompt

See the `$` at the beginning of the line? That's what's known as the shell "prompt." Prompts come in all different flavors, but there's one thing that remains consistent between most default prompts, by convention:

- If it ends in `$`, any commands are being run by a normal user
- If it ends in `#`, any commands are being run by the `root` (or administrator) user

Usually, your prompt will be more complex, something more like this:

```bash
benjamin@archimedes:~/projects/tty1.blog] $
```

A quick rundown of what's going on here:

- `benjamin` - my username
- `archimedes` - the "hostname", or name of the computer I'm using
- `~/projects/tty1.blog` - the path to the "working directory", the current folder I'm inside. Commands run will by default be performed in this folder.

Basically, a prompt is meant to give you information to help contextualize commands you run.

For tty1, I will use the following convention:

- For most commands, I'll use a simple `$` or `#`.
- If it matters what directory I'm in, I'll use `path] $` or `path] #`. For example, `~/projects/tty1.blog] $`.

Keep in mind: the prompt is not part of the command you run. If you're copying a command, *don't include the starting `$` in what you run or you'll get an error*.

<aside>

To be clear: it's a bad idea to copy/paste a command into your terminal. Type it out yourself, and only if you know *exactly* what it does. A malicious command can do anything it wants to your system, which is not good<sup>[[citation needed](https://xkcd.com/285/)]</sup>.

</aside>

### `ls` - The Command

While the whole line you enter in is often referred to as a command, the actual command itself is just the program being run. In this case, that's `ls`, a command which lists files and directories in a directory ("directory" is what most people know as a folder).

### `~/downloads` - Arguments

An "argument" is a piece of information you give to a command to help it do its job. Each command will take a certain number of arguments; `ls` takes one, the path.

<aside>

See the `~` character? That's a special nickname for your "home directory", the folder while all of your files are.

Usually, your home directory will be at `/home/username`; in my case, that's `/home/benjamin`, so for me, `~/downloads` expands to `/home/benjamin/downloads`.

</aside>

Giving a command an argument is called "passing" it the argument. By passing `ls` (which lists the files and directories in a directory) the `~/downloads` path, you're telling it to list for you every file and directory inside the `~/downloads` directory.

For ls, this argument is optional; if you'd left it out, by default it would assume you want a list of the files in the folder you're currently inside, the "working directory".

<aside>

Confused? I'll show you later in this article how to learn a command's arguments if you don't know them already.

</aside>

Commands will sometimes have multiple arguments, in which case they're separated by spaces.

### `--almost-all --sort=extension` - The Flags

Flags specify options or settings to use when running the command. In this case:

- `--almost-all` means to show all files and folders, including ones that would normally be hidden (files and folders starting with `.` are hidden by default), but excluding `.` (referring to the directory itself) and `..` (referring to the parent directory).
- By default, `ls` will sort the results alphabetically. Using `--sort`, you can tell it to sort a different way. Some flags, like `--sort`, allow or require you to pass arguments to them, like you would with commands. Unfortunately, different commands will often have different ways to do this.

Confused yet? There's more. Most programs have multiple versions of each flag. While each command can do this differently, usually flags starting with `--` are "long" flags and ones starting with `-` are short ones which are easier to type out. For example, in the `ls` command, `-A` means `--almost-all` and `-X` means `--sort=extension`. With that in mind, we could have written our command this way:

```bash
$ ls -A -X ~/downloads
```

In fact, you can go even shorter. With one-letter short flags, you can combine them like so:

```bash
$ ls -AX ~/downloads
```

That means the same thing as our original command:

```bash
$ ls --almost-all --sort=extension ~/downloads
```

While short flags are far quicker to type out, I'll avoid using them in this blog, since they're confusing and less understandable than long flags if you don't know what's going on.

<aside>

If you want to pass an argument to a flag, I recommend you use the long version; usually, you'll do that with an equals sign, like above (and if not, just use a space instead of the equals sign). With the short version, it's far more confusing and will sometimes change depending on the command.

</aside>

Options should be listed before the arguments of the command.

## Getting Help

You know how commands work! Well, you know the basics. You're almost certainly going to run into problems and issues, especially since I won't usually be sticking to the basics like I do in this article. Here are some resources that can help with that:

- `man` - gives manual pages for commands. `man ls`, for example, has a full explanation of how to use the `ls` command, including its flags and arguments. This is the most comprehensive way to learn about commands you aren't familiar with, but can sometimes be intimidating. A good way to get started with it and learn how it works is to run `man man` and read the result.
	- `tldr` is a `man` alternative that simplifies the instructions and gives you a few basic examples. See [tldr.sh](https://tldr.sh/) for more information. This is excellent for quick reference, but tends to oversimplify and may not know about every command you want to use. (You'll need to install `tldr`; it doesn't usually come installed on your system.)
- The [ArchWiki](https://wiki.archlinux.org/) is an excellent source of information about anything to do with Linux, even if you're not on Arch (btw)
- **Ask people for help.** Other people are your best bet if you're stuck; many of us have gone through the same issues. If you'd like to ask me, [I'm active on Mastodon](https://fosstodon.org/@benjaminhollon) or you can [contact me directly](https://benjaminhollon.com/contact/).
