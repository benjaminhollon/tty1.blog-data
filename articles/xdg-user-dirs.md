title: Leveling Up Your Filetree with xdg-user-dirs
whoami: benjamin
published: 2023-05-08T02:21:59+00:00
mastodonThread: https://fosstodon.org/@benjaminhollon/110330710555645260
tags:
- xdg-user-dirs
- directory-organization

---

Have you ever noticed that there are certain directories everyone has? `~/Documents`, `~/Downloads`, `~/Desktop`, and so forth? Some of them you don't need, some of them you might wish were named differently, but any time you rename or delete them, the originals *reappear*?

You see, these directories follow a standard so that all programs know where they are—with the right tools under your belt, you can customize them.

---

Have you ever noticed that there are certain directories everyone has? `~/Documents`, `~/Downloads`, `~/Desktop`, and so forth? Some of them you don't need, some of them you might wish were named differently, but any time you rename or delete them, the originals *reappear*?

You see, these directories follow a standard so that all programs know where they are—with the right tools under your belt, you can customize them.

## Meet `xdg-user-dirs`

[`xdg-user-dirs`](https://freedesktop.org/wiki/Software/xdg-user-dirs/) is a set of two utilities that manages the placement of standard directories on your installation. Most people already have it, though they may not know about it, since it comes with many major Desktop Environments.

If you don't have it, go ahead and install it now.

To get started, run this command:

```bash
$ xdg-user-dirs-update
```

This command creates all of the configured folders on your system. If it's not already configured, it will create the configuration file for you.

By default, these are the folders it creates:

- `~/Desktop`
- `~/Documents`
- `~/Downloads`
- `~/Music`
- `~/Pictures`
- `~/Public`
- `~/Templates`
- `~/Videos`

Now that everything's set up, let's customize these folders to our taste.

## Customizing Your User Directories

There are two configuration files to worry about. If you want to configure the default folders for every user on your installation, you can edit `/etc/xdg/user-dirs.defaults`. To configure user-specific folders, edit `~/.config/user-dirs.dirs`.

<aside>

You can actually customize the location of configuration files! The `$XDG_CONFIG_HOME` environment variable controls this; it defaults to `~/.config`. If you have it customized to, say, `~/configuration`, the user-dirs configuration file would be at `~/configuration/user-dirs.dirs` instead.

We'll be going over how to customize your shell and environment variables in a few weeks, so stay tuned, if you don't know how.

</aside>

By default, you get this:

```bash
# This file is written by xdg-user-dirs-update
# If you want to change or add directories, just edit the line you're
# interested in. All local changes will be retained on the next run.
# Format is XDG_xxx_DIR="$HOME/yyy", where yyy is a shell-escaped
# homedir-relative path, or XDG_xxx_DIR="/yyy", where /yyy is an
# absolute path. No other format is supported.
# 
XDG_DESKTOP_DIR="$HOME/Desktop"
XDG_DOWNLOAD_DIR="$HOME/Downloads"
XDG_TEMPLATES_DIR="$HOME/Templates"
XDG_PUBLICSHARE_DIR="$HOME/Public"
XDG_DOCUMENTS_DIR="$HOME/Documents"
XDG_MUSIC_DIR="$HOME/Music"
XDG_PICTURES_DIR="$HOME/Pictures"
XDG_VIDEOS_DIR="$HOME/Videos"
```

In itself, this is pretty self-explanatory. Let's do some simple customization and make all of these directory names lowercase, for easier typing in the terminal.

```bash
XDG_DESKTOP_DIR="$HOME/desktop"
XDG_DOWNLOAD_DIR="$HOME/downloads"
XDG_TEMPLATES_DIR="$HOME/templates"
XDG_PUBLICSHARE_DIR="$HOME/public"
XDG_DOCUMENTS_DIR="$HOME/documents"
XDG_MUSIC_DIR="$HOME/music"
XDG_PICTURES_DIR="$HOME/pictures"
XDG_VIDEOS_DIR="$HOME/videos"
```

Now, you could go ahead and lock this in, but this would only create new directories in the new locations; it doesn't move the current folders. Let's do that first, so that we don't have a bunch of extra directories.

```bash
mv ~/Desktop ~/desktop
mv ~/Downloads ~/downloads
mv ~/Templates ~/templates
mv ~/Public ~/public
mv ~/Documents ~/documents
mv ~/Music ~/music
mv ~/Pictures ~/pictures
mv ~/Videos ~/videos
```

<aside>

I've removed the shell prompts (`$`) I usually add to commands for the sake of making this easier to copy/paste. That said, please remember never to copy commands you don't fully understand into the terminal.

</aside>

Now that our files are in the right place, let's update our user directory listings:

```bash
$ xdg-user-dirs-update
```

Now, any compatible program will automatically use the new directories you've specified. Hooray!

### Removing Default Directories

Sometimes you don't want one of the directories that `xdg-user-dirs` adds by default. I, for example, don't have any need for `~/desktop`. Well, let's try removing it:

```bash
XDG_DOWNLOAD_DIR="$HOME/downloads"
XDG_TEMPLATES_DIR="$HOME/templates"
XDG_PUBLICSHARE_DIR="$HOME/public"
XDG_DOCUMENTS_DIR="$HOME/documents"
XDG_MUSIC_DIR="$HOME/music"
XDG_PICTURES_DIR="$HOME/pictures"
XDG_VIDEOS_DIR="$HOME/videos"
```
```bash
$ rmdir ~/desktop
$ xdg-user-dirs-update
$ ls
Desktop documents download music pictures public templates videos
```

Oops, it reappeared, in the default form. If we check back in our config file, we'll find this added to the end:

```bash
XDG_DESKTOP_DIR="$HOME/Desktop"
```

Okay, so `xdg-user-dirs` won't let us remove the default directories. Why? Because there are programs that rely on those folders existing, and `xdg-user-dirs` doesn't want them to be missing when those programs go looking.

So, is there a workaround?
Well, let's think about this use-case. What do programs usually want to know about the desktop for? Usually so they can put a file or link there for quick access. For me, that's what the `downloads` folder tends to be for. Let's try something:

```bash
XDG_DESKTOP_DIR="$HOME/downloads"
```

…and it works, even though `XDG_DOWNLOAD_DIR` is already set to `$HOME/downloads`. You can double up like this, which I find handy.

You can also set one to a subdirectory:

```bash
XDG_TEMPLATES_DIR="$HOME/documents/templates"
```

### Adding Extra User Directories

Perhaps you want an extra directory defined, besides what the defaults are. I, for example, have extra user directories for my writing, screenshots, and notes:

```bash
XDG_WRITING_DIR="$HOME/writing"
XDG_SCREENSHOTS_DIR="$HOME/pictures/screenshots"
XDG_NOTES_DIR="$HOME/notes"
```

While these aren't used by any programs I use, I can reference them in my own scripts. For example, if I have multiple scripts that need to access my notes, I no longer have to update each one if I decide to move my notes; all I need to do is update `XDG_NOTES_DIR` and run `xdg-user-dirs-update`.

How do you reference these directories? Let's learn.

## Using `xdg-user-dir`

To get the location of a user directory, use the `xdg-user-dir` command:

```bash
$ xdg-user-dir NOTES
/home/benjamin/notes
```

Want to include this as part of a command? Say, for example, that you have this in a script:

```bash
echo "You have $(ls ~/notes/school | wc -l) notes from class"
```

Notice the `$()`; anything inside the parentheses is replaced with its output when run as a command. So, let's use this to update the script:

```bash
echo "You have $(ls "$(xdg-user-dir NOTES)/school" | wc -l) notes from class"
```

*Et voila!* Now, whenever you move your notes and update the user-dir configuration, any scripts using this strategy will automatically work with the new location.

## Closing Thoughts

You don't have to mess around with your user directories. Much of what I've talked about here may not be interesting to you.

But having the knowledge of what goes on behind the scenes? That's valuable. And who knows, perhaps you'll find a time when this comes in handy. To me, it's helped give a more personal touch to my file organization, letting me do things my way rather than the way they came by default.

Go forth and be awesome.

<aside>

In case it helps anyone, here's how my directories are configured, as of this writing:


```bash
XDG_DOWNLOAD_DIR="$HOME/downloads"
XDG_LINKS_DIR="$HOME/links"
XDG_DOCUMENTS_DIR="$HOME/documents"
XDG_WRITING_DIR="$HOME/writing"
XDG_MUSIC_DIR="$HOME/music"
XDG_PODCAST_DIR="$HOME/podcasts"
XDG_PICTURES_DIR="$HOME/pictures"
XDG_SCREENSHOTS_DIR="$HOME/pictures/screenshots"
XDG_VIDEOS_DIR="$HOME/videos"
XDG_TEMPLATES_DIR="$HOME/templates"
XDG_PUBLICSHARE_DIR="$HOME/public"
XDG_SCHOOL_DIR="$HOME/school"
XDG_NOTES_DIR="$HOME/notes"
XDG_DESKTOP_DIR="$HOME/downloads"
```

Also, do you remember my [templates trick](https://tty1.blog/articles/no-file-manager/#creating-files) from the last article? That's a good use of `XDG_TEMPLATES_DIR`.

</aside>
