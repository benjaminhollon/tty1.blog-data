title: You Don't Need a File Manager
whoami: benjamin
published: Sat Apr  8 03:17:32 PM CDT 2023
mastodonThread: https://fosstodon.org/@benjaminhollon/110165035176608816
tags:
- file-management
- terminal-replacements

---

A file manager feels like an essential part of an operating system. In a blog where I talk largely about using terminal applications instead of GUIs, you might think I'd spend this article exploring some terminal-based file managers.

Not so. Spare me a moment of your time, friend, and I will endeavour to illuminate why you, master of command-line secrets, have no need for such a petty thing as a "file manager."

---

A file manager feels like an essential part of an operating system. In a blog where I talk largely about using terminal applications instead of GUIs, you might think I'd spend this article exploring some terminal-based file managers.

Not so. Spare me a moment of your time, friend, and I will endeavour to illuminate why you, master of command-line secrets, have no need for such a petty thing as a "file manager."

## Replacing Functionality

Let's walk through the different things traditional file managers do and figure out new ways to do them with regular terminal commands.

### Listing Files

If you know anything about the terminal, you should know this command. (If you don't, [I pick it to pieces here](https://tty1.blog/articles/anatomy-of-a-command/).)

```bash
$ ls
```

### Creating Files

If you want to create an empty text file, you can do this easily with `touch`:

```bash
$ touch ~/notes/things-to-do-today.md
```

To create a directory, use `mkdir`:

```bash
$ mkdir ~/notes/top-secret
```

If `~/notes` doesn't already exist, this will throw an error. To create all necessary folders (and not just the final one), use the `--parents`/`-p` flag.

```bash
$ mkdir --parents ~/notes/top-secret
```
```bash
$ mkdir -p ~/notes/top-secret
```

If you're wanting to create files from templates, that's also pretty easy. First, create a folder to store your templates in.

```bash
$ mkdir ~/templates
```

Whenever you have a new type of file you want to make a template for, create a file that's a starting point for the type in this `templates` folder. Then, when you want to create a new file with that template, do it like so:

```bash
$ cp ~/templates/tty1-post.md ./no-file-manager.md
```

### Opening Files

This one seems a bit tricky, but it's easy once you have it figured out. If you want to open a file in the default program, use `xdg-open`, from `xdg-utils`.

```bash
$ xdg-open <file>
```

*Setting* the default program for a file type is a bit trickier. `xdg-mime` is the standard way to do so (see `man xdg-mime`), but I prefer to use a utility called [`mimeopen`](https://github.com/mbeijen/File-MimeInfo).

```bash
$ mimeopen <filename>
```

If you already have a program set to open the file in, it'll just open it. If not, it'll ask you to pick the program. If you want to force it to ask you even though you've set the default application, use the `--ask-default`/`-d` flag:

```bash
$ mimeopen --ask-default <filename>
```

I set an alias for this to make it even easier:

```bash
alias open="mimeopen"
```

Now running `open <file>` will open `<file>` with the default program, asking which to use if you haven't set it.

<aside>

`mimeopen` may or may not be easy for you to install depending on the Linux distribution you're using. It's been hit or miss for me.

If you're up for [setting up nix](https://nixos.org/), you can get `mimeopen` from there even if your distro doesn't package it (the package is `perlPackages.FileMimeInfo`). Or, if you prefer, build it yourself from source.

</aside>

### Moving Files

Moving files and directories is pretty easy.

```bash
$ mv ~/downloads/download.pdf ~/documents
```

This will move `download.pdf` from `~/downloads` to `~documents`.

### Renaming Files

This solution is actually the same. You can use `mv` to rename files and folders.

```bash
$ mv untitled.md world-domination-plan.md
```

You can even move and rename at the same time:

```bash
$ mv ~/downloads/untitled.md ~/notes/top-secret/world-domination-plan.md
```

However, you'll often want to rename multiple files in the same directory. There are a lot of tools for that, but I've settled on `edir`, myself.

```bash
~/folder]$ edir 
```

This will open a list of the files in the directory in my default text editor:

```bash
1	./untitled
2	./untitled (1)
3	./untitled (1) (1)
4	./untitled (1) (1) (1)
5	./untitled (1) (1) (1) (1)
6	./untitled (1) (1) (1) (1) (1)
7	./untitled (1) (1) (1) (1) (1) (1)
```

Not very pretty, huh? Now that it's in my text editor, though, I can edit the names of any of those files here.

```bash
1	./cool-file
2	./secret-plans
3	./birthday-present-ideas
4	./cookie-recipe
5	./ultimate-brownies
6	./short-story
7	./public-speaking-notes-03-31-2022
```

When I save and exit, `edir` will apply the changes for me!

```bash
~/folder]$ edir
Renamed untitled to cool-file
Renamed untitled (1) to secret-plans
Renamed untitled (1) (1) to birthday-present-ideas
Renamed untitled (1) (1) (1) to cookie-recipe
Renamed untitled (1) (1) (1) (1) to ultimate-brownies
Renamed untitled (1) (1) (1) (1) (1) to short-story
Renamed untitled (1) (1) (1) (1) (1) (1) to public-speaking-notes-03-31-2022
~/folder]$ ls
birthday-present-ideas  cookie-recipe  cool-file  public-speaking-notes-03-31-2022  secret-plans  short-story  ultimate-brownies
```

How helpful this is will depend partly on what text editor you use. For me, in `neovim`, editing filenames this way is *far* easier than repeatedly using `mv`.

<aside>

`edir` may be packaged in your distribution, but if not it's available through pip:

```bash
$ sudo pip3 install -U edir
```

For more information see [its repository](https://github.com/bulletmark/edir).

</aside>

### Deleting Files

Deleting files is easy.

```bash
$ rm ~/unwanted-file.pdf
```

Or delete all the files in a folder.

```bash
$ rm ~/downloads/*
```

Deleting a directory is a little trickier. `rmdir` will do the trick if it's empty.

```bash
$ rmdir ~/junk-folder/
```

You can also use the `--parents`/`-p` flag, like in `mkdir`. However, if you want to remove a directory and everything in it, you'll need `rm` again, this time with the `--recursive`/`-r` flag.

```bash
$ rm --recursive ~/junk-folder/
```

<aside>

I'm gonna give you a quick tip right now: if anyone ever tells you to run `sudo rm -rf /` or any variant of it, __*don't*__.

</aside>

Now, `rm` permanently deletes files. While this can be what you want, this isn't what a file manager usually does; it would move deleted files to the "trash." Well, so can you, with [`trash-cli`](https://github.com/andreafrancia/trash-cli), a suite of commands to help you manage your trash!

I'm going to give some quick examples below, but for full instructions use the manpages for the commands.

#### Trash files or folders

```bash
trash <path>
```

<aside>

I preferentially use this over `rm` because it works for files and directories, empty or no.

</aside>

#### Empty trash older than `days`

```bash
trash-empty [days]
```

You'll want to do this fairly regularly or you won't be saving any storage space. I personally run `trash-empty 7` once every couple days.

#### Restore trashed files

```bash
trash-restore
```

This will walk you through the process.

## Advanced: Mounting Drives

Now, I could go into the `mount` command and how to use it, but if you want something that complex and nuanced, you can read `man mount` yourself. [I already went into a complicated command in-depth recently](https://tty1.blog/articles/how-to-save-the-world-with-tar/) and I don't feel like doing it again here. (The `mount` manpage is 80% longer than the `tar` manpage.)

Instead, I'd like to point you to `udisks2`. It provides the handy `udisksctl` utility.

Once you have `udisks2`, though, you have an even easier simpler solution: [`udiskie`](https://github.com/coldfix/udiskie). Install it, set it running, and any drives you attach will automatically be mounted to your filesystem. Find the results in the `/run/media/<your-username>/` directory.

Your desktop environment might already be set up to automatically mount drives, in which case you can ignore this section. If you're using a barebones window manager like me, though, this could come in handy.

## Honorable Mentions: Terminal-Based File Managers

Despite all this, there are some great terminal file managers out there. Here are some I've heard of, in no particular order.

- `vifm`
- `ranger`
- `nnn` (I have actually tried this one)
- `lf`

I don't use any of these, so I don't make any guarantees of quality, but I have heard good things from people who do. Consider giving them a shot, but at least try to go terminal-only first; you have what it takes!

## Conclusion

Surely by this point you have been convinced. You, discriminating command-line magician that you are, have no need for the crutch that a file manager is.

Unleash your inner terminal nerd and manage your files with your terminal only! (At the very least, you'll get some sweet, sweet, nerd cred.) Go forth and be awesome.
