title: Introduction
whoami: benjamin
mastodonThread: https://fosstodon.org/@benjaminhollon/110078783514140910
published: Fri Mar 24 09:35:57 AM CDT 2023
tags:
- meta
- introduction
- series-basics

---

Welcome to tty1, a blog about the Linux terminal! Get started on your journey with this article.

---

Welcome to tty1, a blog about the Linux terminal!

## Who are you, and what is this all about?

Easy to answer the first question:

```bash
$ whoami
benjamin
```

I kid. Let me introduce myself for real.

```bash
$ cat about-$(whoami).toml
name = "Benjamin Hollon"

website = "benjaminhollon.com"
code = "codeberg.org/benjaminhollon"
mastodon = "@benjaminhollon@fosstodon.org"

occupation = "university student"
major = "Communications (BA)"
university = "Texas A&M"

hobbies = [
	"writing"
	"coding"
	"astronomy"
	"playing trombone"
	"composing music"
	"reading"
]
dream = "developing and supporting Free and Open Source software for the writing industry"
terminal_emulator_of_choice = "foot"

citizenship = "United States of America"
lived = [
	"Afghanistan"
	"India"
	"Malaysia"
	"United States of America"
]
```

Is this a nerdy way to introduce myself? Yes. If that's not what you're looking for, I'm sorry to burst your bubble, but you're in the wrong place. This is a nerdy blog for nerdy people.

As for what this is about, tty1 is a blog where I'll share tips, tricks, guides, reviews, and anything and everything related to the Linux terminal. If you don't know what that means, again, you're in the wrong place.

Eventually, this blog may feature [guest articles](https://tty1.blog/guest-authors/) by people who know their stuff; we'll be starting off with articles by yours truly as I get into stride and establish the proper tone and feel for this blog.

### Why the name `tty1`?

Long story short, a `tty` is a terminal for your Linux-based operating system that doesn't require a graphical system like X or Wayland.

Originally, I was going to write this blog about my attempt to daily-drive a Linux installation without a graphical system, doing everything from the `tty`, hence the title. I've since broadened it to be about the Linux terminal in general, but the name still fits.

## Housekeeping

Let's get a few things straight before we begin our grand tour of the Linux terminal.

### Subscribing for updates

To get updates, you can use an RSS feed reader, subscribing to the link [https://tty1.blog/feed/](https://tty1.blog/feed/). You can also open the link to see a preview of the feed. For an explanation of what RSS is, check out [About Feeds](https://aboutfeeds.com).

For a good terminal client to check your RSS feeds, I highly recommend [newsboat](https://newsboat.org/). That's the client I use. The website has a comprehensive guide and of course you can always run `man newsboat` to get information on using it.

### Contacting Me

There are [a few ways to contact me](https://benjaminhollon.com/contact/) on my site, but if you're not wanting a private conversation (in which case email is best), I'd love if you reach out to me on [Mastodon](https://fosstodon.org/@benjaminhollon)!

I'm happy to take feedback, topic recommendations, questions, cookie recipes, and anything else on your mind.

### Why the "Linux" terminal?

Other operating systems may have great terminals, but I use Linux. Some of what I talk about will apply to other operating systems, but I'm not going to make any guarantees; do your own research to make sure.

## Thank you!

With all of this out of the way, we'll be kicking this blog off with a deep dive into the anatomy of a command.
