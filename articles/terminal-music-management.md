title: Terminal-Based Music
whoami: benjamin
published: 2023-06-10T14:06:55+00:00
mastodonThread: https://fosstodon.org/@benjaminhollon/110520315037873170
tags:
- my-setup
- beets
- gonic
- abcde
- cmus
- yt-dlp
- youtube-dl
- music

---

About a year ago, I stopped using Spotify for my music, instead moving to local audio files. I've never regretted that decision. When I started transitioning to a terminal-based workflow, though, I had to find good utilities to manage and play my collection.

I think that the results have been far more useful than the graphical apps I used to use were.

---

About a year ago, I stopped using Spotify for my music, instead moving to local audio files. I've never regretted that decision. When I started transitioning to a terminal-based workflow, though, I had to find good utilities to manage and play my collection.

I think that the results have been far more useful than the graphical apps I used to use were.

## Acquiring Music

My preferred method of acquiring music is to buy used CDs and rip them. My tastes in music are fairly old, mostly classical, big band, etc., so I can get away with this. I go to Half Price Books and buy up a bunch of clearanced CDs for 50¢ each.

You may decide to pursue a "less legal" solution, but I won't comment on that, except to point out a helpful alias I have set up for [`yt-dlp`](https://github.com/yt-dlp/yt-dlp), to download audio from YouTube:

```bash
alias yt-audio='yt-dlp -x --audio-format mp3'
```

Then I can simply run `yt-audio [link to song or playlist]` to download a song or album to the current directory.

On to what I do with my CDs. I've settled on [`abcde`](https://abcde.einval.com/wiki/). Once again, I've set up an alias to automatically apply settings I want.

```bash
alias rip='abcde -Vx -o flac'
```

- `-V` - Makes the output more verbose, making sure I get all status updates. I like verbose output. :)
- `-x` - Ejects the disk once `abcde` is finished.
- `-o flac` - Outputs the files in the `flac` format

I `cd ~/downloads`, run `rip`, and `abcde` creates a subfolder with all the files, trying to see if it can find information on the album and adding metadata if it does.

## Managing the Library

The magic of my music management happens through [`beets`](https://beets.io/), which bills itself as "the music geek's media organizer". If you're interested in my configuration, I have it posted [in my dotfiles](https://codeberg.org/benjaminhollon/.dotfiles/src/branch/main/.config/beets/config.yaml). Basically, it sets where I store my music, says to move music files rather than just copying them, and gets album art when importing.

Once I've ripped a bunch of CDs into `~/downloads` with `abcde`, I run `beet im .` in `~/downloads` to import all the albums. It automatically looks for album metadata from [MusicBrainz](https://musicbrainz.org), adds it to the files, then sorts them into your music collection. Mine is set up to be at `~/music`.

If you want to learn all of `beets`'s features, it has [online documentation](https://beets.readthedocs.io/en/stable/index.html) available. There are also manpages: `man beet` for the CLI and `man beetsconfig` for information on configuring `beets`.

## Playing Music

I play music through [`cmus`](https://cmus.github.io/). It's pretty self-explanatory; if you don't understand something, see `man cmus`. You'll probably want to take a look there at first to find all the default keybindings.

One thing you will have to do is add music from your collection. Go to the "Browser" view with `6`, use arrows or `j` and `k` to highlight the directory you store your music in, then use `a` to add everything in it to your Library (at which point it will be viewable in the default view; you can get back there with `1`). You'll want to do this again whenever you add get new music.

## Getting Metadata

I sometimes want to see what's playing without going over to `cmus`. In SwayWM, my window manager, I can set it to run commands or scripts when a keyboard shortcut is triggered.

I use `playerctl` to get information about what's currently playing, which has worked for most music players I've tried. Here's my custom `notify-playing` script:

```bash
#! /bin/sh
metadata=$(playerctl metadata)
title=$(echo "$metadata" | grep ":title\b" | awk 'BEGIN{FS=" {2,}"}{ print $3 }')
artist=$(echo "$metadata" | grep ":artist\b" | awk 'BEGIN{FS=" {2,}"}{ print $3 }')
album=$(echo "$metadata" | grep ":album\b" | awk 'BEGIN{FS=" {2,}"}{ print $3 }')
artUrl=$(echo "$metadata" | grep ":artUrl\b" | awk 'BEGIN{FS=" {2,}"}{ print $3 }')

notify-send "$title" "$artist – $album" -i "$artUrl" -t 5000 || notify-send "Nothing playing." -t 5000
```

Put it in a folder in your `$PATH`. Now, when `notify-playing` is run, you get a notification with the title, album, and artist of the song playing.

<aside>

`cmus` does not include `artUrl` in its metadata, but I put that in anyway in case I ever use a player that does. I think there's also a script you can add to cmus to add support for `artUrl`, though I haven't tried it.

Also, to give credit where it's due, [R.L. Dane](https://rldane.space/) helped a bit with this script, specifically suggesting a better Regular Expression for awk to use in parsing the metadata.

</aside>

## Bonus: Streaming Music

While it's not strictly for the terminal, I use [`gonic`](https://github.com/sentriz/gonic) to stream music to my phone from my collection, since the collection is too large to store full-quality copies on my phone. I won't go into all the details, but it's a streaming backend that works well with `beets`, so I thought I'd mention it.

## Conclusion

This whole workflow won't be perfect for everyone. Still, parts of it can be integrated into whatever your ideal music setup is. Specifically, I think `beets` is a tool can fit into and improve any setup.

Crafting your music setup is a journey, and it's different for everyone. Once you've figured out the specifics of your setup, do let me know! I'm happy to compare notes and perhaps learn some tips myself.

Happy listening!
