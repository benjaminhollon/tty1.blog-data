title: How to Save the World with tar
whoami: benjamin
published: Fri Mar 31 06:13:41 PM CDT 2023
mastodonThread: https://fosstodon.org/@benjaminhollon/110120450626056754
tags:
- tar
- command-breakdown
- xkcd-reference

---

While there are hundreds of difficult-to-understand commands out there, few have acquired the *notoriety* of `tar`. Most guides are filled with abstruse commands such as `tar -czvf archive.tar.gz ~/Downloads/` with little to no explanation. Based on conversations I've had, people who interact with `tar` tend to just memorize a basic command or too, like `tar -xvf <filename>` and hope for the best.

Let's take a closer look at `tar` and learn how it actually works.

---

While there are hundreds of difficult-to-understand commands out there, few have acquired the *notoriety* of `tar`. Most guides are filled with abstruse commands such as `tar -czvf archive.tar.gz ~/Downloads/` with little to no explanation. Based on conversations I've had, people who interact with `tar` tend to just memorize a basic command or two, like `tar -xvf <filename>` and hope for the best.

<figure>

<img alt="xkcd#1168; full transcript at https://explainxkcd.com/1168/" src="https://imgs.xkcd.com/comics/tar.png" title="I don't know what's worse--the fact that after 15 years of using tar I still can't keep the flags straight, or that after 15 years of technological advancement I'm still mucking with tar flags that were 15 years old when I started.">

<figcaption>

[xkcd#1168](https://xkcd.com/1168/) by Randall Munroe

</figcaption>

</figure>

Let's take a closer look at `tar` and learn how it actually works.

## A Quick Summary

`tar` is used for creating and interacting with archive files; `.tar` archive are similar in functionality to `.zip` files, which you may be more familiar with.

The name "tar" is short for "tape archives"; the original idea behind tar is that the archives in question would be stored on tape. In contemporary usage, that's not usually the case.

## The "Easy" Way to Learn

The best explanation of `tar` is in its manual page.

```bash
$ man tar
```

However, this `man` page is extremely long and most people skip it altogether. Still, if you want a deeper understanding than what this guide will give you, the `man` page is the place to go.

## Usage Styles

### "Traditional" Usage

```bash
$ tar xvf archive.tar.gz
```

I won't cover this style in this article. Most of the basics are similar to Unix-style flags, and any other information you need to know is explained within `man tar` under `Description ⇒ Option Styles`. It's not the style I recommend you use, but it may be handy to understand if you see it in another guide.

### "Unix-style" Usage

```bash
$ tar -xvf archive.tar.gz
```

These are standard [short flags](https://tty1.blog/articles/anatomy-of-a-command/#--almost-all---sortextension---the-flags); you can use them individually (`-x -v -f`) or combine them (`-xvf`). This is the form you'll usually see used.

### "GNU-style" Usage

```bash
$ tar --extract --verbose --file=archive.tar.gz
```

I think this is the most straightforward way to use `tar`; it's much clearer what the different flags mean and which arguments are passed to which flags. However, it's much harder to type out, so it isn't usually used.

Something many people don't know, though, is that you don't have to type out the full flags; you can just use the first letters, as long as the letters you type are unique to one flag.

```bash
$ tar --ext --verbo --file=archive.tar.gz
```

If you shorten a flag too far and there are multiple possibilities, `tar` will warn you and give you the possible completions.

## Flag Walkthrough

Let's go through some of the most useful flags one by one and learn how they're used.

### `--verbose`/`-v`

The `--verbose` flag just makes sure that you'll see all output from the command you run; by default, most `tar` commands will just do everything without returning any output, making it hard to tell if anything is happening, especially when a command takes a long time to run.

As a general rule, I add `--verbose` to almost every `tar` command I run; I like seeing what's going on. It will, though, spit out a *ton* of information, filling up your terminal screen, which might not be what you want.

### `--extract`/`-x`

This flag says to extract the contents of an archive; you'll need to use the `--file` flag for it to do anything, which we'll learn about next.

Optionally, it takes arguments to specify *which* files to extract.

### `--file`/`-f`

This flag specifies the archive file that you'll be working with, whether you're creating it, reading from it, adding files, etc. It takes one argument, the location of the archive file.

This is a good time to talk about how to pass arguments in `tar`. The long flags are the easiest to pass to:

```bash
$ tar --extract --file=archive.tar.gz
```

You can use a space instead of the `=`, but I suggest you use `=` for clarity.

For short flags, the argument value must follow the flag it's being passed to. This can take multiple forms:

```bash
$ tar -f archive.tar.gz -xv
```

```bash
$ tar -xvf archive.tar.gz
```

### `--list`/`-t`

This flag says to list everything in an archive specified by `--file` or `-f`.

```bash
$ tar --list --file=archive.tar.gz
```
```bash
$ tar -tf archive.tar.gz
```

I don't generally bother including `-v` with this, since it'll be outputting the contents of the archive anyway.

Optionally, this takes arguments to specify which files to list.

### `--create`/`-c`

This flag says to create a new `tar` archive with the filename and location specified by `-f` or `--file`. It takes one or multiple arguments to specify which files to add to the new archive.

```bash
$ tar --create=~/school --file=school.tar --verbose
```
```bash
$ tar --create ~/school ~/Downloads ~/writing --file=archive.tar --verbose
```
```bash
$ tar -cvf archive.tar ~/projects
```

Note that arguments passed to the action flag (such as `-c`, `-t`, `-x`) can be put at the end of the command, after all the other flags, as shown in the last example above. I couldn't find any specific rationale for that in the `tar` manpage, but it works and people often do it that way.

### `--gzip`/`-z`

`--create` alone doesn't apply any compression to files; to do that, you need to select a compression method. I personally always use gzip, which is applied with the `--gzip` or `-z` flag.

```bash
$ tar -czvf archive.tar.gz ~/Downloads
```

<aside>

Why not always do this? Well, `tar` takes a significant performance hit when applying compression, and it doesn't always save much space.

</aside>

To use a different compression type, swap out this flag for the flag of the type you want to use. By convention, an extra file extension is appended to compressed archives, depending on the type of compression.

- `--gzip`/`-z` - `gzip` compression, uses `.tar.gz`
- `--bzip2`/`-j` - `bzip2` compression, uses `.tar.bz2`
- `--xz`/`-J` - `xz` compression, uses `.tar.xz`

There are some others too, but you get the idea. See `man tar` for a full list of compression methods.

<aside>

There's another way to do compression that you might find easier; use `--auto-compress`/`-a` to automatically detect how to compress the file based on the filename you chose. For example:

```bash
$ tar -cavf archive.tar.xz ~/Downloads
```

This will notice the `.tar.xz` ending to the archive name and automatically use `xz` compression.

</aside>


### `--append`/`-r`

This will add files to an archive. Use it the same way you would `--create`/`-c`; use `--file`/`-f` to specify the archive, then pass directory and file paths to it to have them added to the archive.

```bash
$ tar -rvf school.tar.gz ~/Downloads/syllabus.pdf
```

### More

There are *many* other flags for `tar`, some of which may be useful to you. The goal of this article was largely to get you to the point where reading the manual page will actually be helpful rather than just daunting. If you need to do something you don't know how to with `tar`, here's what I'd recommend:

1. Begin by searching up what you want to do and finding a guide for it. Unfortunately, most guides to `tar` aren't very conducive to actually *understanding* what you're doing, just rote memorization.
2. Take any flags you don't understand from what you find and look for them in `man tar` to get full instructions for how to use them. The manpage is actually pretty helpful if you're just looking up flags with a basic preexisting knowledge of how `tar` works.

## Practice

Let's try this out; I'll begin by giving you some example commands with `tar` (using short flags, since the long ones are pretty easy). See if you can figure out what they do, then look at the explanation to test your knowledge.

<hr>

```bash
$ tar -xvf archive.tar.gz
```

<details><summary>Show Explanation</summary>

- `-x` - this is the action; this command will be extracting all contents of an archive (since specific contents were not passed to the argument)
- `-v` - the command will be spitting out extra information as a status report
- `-f` - this gets passed the archive we're working with; in this case, `archive.tar.gz` is the archive. (Note that it is compressed using gzip.)

The final result? The archive gets decompressed to the current directory. If you're going to remember one `tar` command, this is a good one; it will always extract the archive you specify, regardless of compression.

</details>

<hr>

```bash
$ tar -xvwf archive.tar
```

<details><summary>Show Hint</summary>

You're going to need to test your manpage-searching skills for this one!

</details>

<details><summary>Show Explanation</summary>

This is *almost* the same as the last one; the only difference is the added `-w` flag. If you checked `man tar`, you know that this flag means that `tar` will ask for individual confirmation before extracting each file.

</details>

<hr>

```bash
$ tar -cavf backup.tar.gz ~/Documents
```

<details><summary>Show Explanation</summary>

- `-c` - creates an archive
- `-a` - automatically detects what compression to use based on the filename of the archive
- `-v` - use verbose output
- `-f` - sets the filename for the archive, `backup.tar.gz`—note that this will make the archive use gzip compression, due to the `-a` flag and the file extension `.tar.gz`
- `~/Documents` - Makes `~/Documents` the content of the new archive

</details>

## Conclusion

Congratulations, you know enough about `tar` to save the world from nuclear destruction! Go forth and use your newfound knowledge for good, not evil. (Or, you know, if you want to use it for evil, that's up to you, I suppose.)

See you next time; we'll be considering whether file managers are *really* necessary.
